Database = [
    ["Plat_No", "Brand", "Year", "Prize", "Status"],
    ["____________","____________","____________","____________","____________"],
    ['B1090ABD', 'BMW', 2010, 400000, 'AVAILABLE'],
    ['B2340EGS', 'TESLA', 2011, 300000, 'AVAILABLE'],
    ['B3457THR', 'TOYOTA', 2012, 500000, 'NOT AVAIL'],
]
Cart = [
    ["Plat_No", "Brand", "From", "Until", "Prize", "Status"],
    ["____________","____________","____________","____________","____________","____________"],
]
printFormat = "{:<2}" + "{:<12} | " * (len(Database[0]))