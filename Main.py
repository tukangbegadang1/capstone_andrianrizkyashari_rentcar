import re
import sys
from Data import *
Total = 0
def ShowMenu():
    print('''
    List Menu:
    1. Menampilkan daftar kendaraan
    2. Buat Data Kendaraan Baru
    3. Ubah Data kendaraan
    4. Menghapus kendaraan
    5. Sewa kendaraan
    6. Pembayaran
    7. Exit
    ''')
    Menu = int(input("Masukan angka menu : "))
    return Menu

def ChooseMenu():
    F = True
    while F == True:
        Menu=ShowMenu()
        F=False

        if Menu == 1:
            print('''
                1. Tampilkan semua data
                2. Tampilkan data berdasarkan nomor kendaraan
            ''')
            try :
                pilihan_data=int(input("Masukan menu yang anda inginkan : "))
                ShowData(pilihan_data)
            except:
                print('Menu yang anda pilih tidak tersedia')
                ChooseMenu()
            F=True
        elif Menu == 2:
            ShowData('AddToDatabase')
            AddToDatabase()
            F=True
        elif Menu == 3:
            ShowData('EditDatabase')
            EditDatabase()
            F=True
        elif Menu == 4:
            ShowData('DeleteDatabase')
            NK = str(input("Masukan nomor kendaraan yang ingin di hapus : "))
            DeleteDatabase(NK)
            F=True
        elif Menu == 5:
            ShowData('AddToCart')
            AddToCart()
            F=True
        elif Menu == 6:
            ShowData('Payment')
            Payment(Total)
            break
        elif Menu == 7:
            valid = str(input('Yakin ingin keluar ? (Y/N)')).upper()
            if valid == 'Y':
                print('Sampai Jumpa!')
                sys.exit()
            else :
                ChooseMenu()
        else:
            print('Menu yang anda pilih tidak tersedia')
            ChooseMenu()
            
def ShowData(value):
    if value == 1:
        print("\nDatabase Kendaraan\n")
        for row in Database:
            a=print(printFormat.format("|", *row))
        return a, ChooseMenu()
    elif value == 2:
        data = str(input("\nMasaukan Nomor Kendaraan : "))
        data = data.upper()
        data = re.sub("\s",'', data)
        for i in range(len(Database)):
            if Database[i][0] == data:
                a = f'\nData yang anda cari:\nNomor Kendaraan : {Database[i][0]}\nBrand : {Database[i][1]}\nYear : {Database[i][2]}\nPrize : {Database[i][3]}\nStatus : {Database[i][4]}\n'
                break
            else:
                a = "Data Tidak Ditemukan"
        return print(a), ChooseMenu()
    elif value == 'EditDatabase':
        for row in Database:
            a=print(printFormat.format("|", *row))
        return a
    elif value == 'AddToDatabase':
        for row in Database:
            a=print(printFormat.format("|", *row))
        return a
    elif value == 'DeleteDatabase':
        for row in Database:
            a=print(printFormat.format("|", *row))
        return a
    elif value == 'AddToCart':
        for row in Database:
            a=print(printFormat.format("|", *row))
        return a
    elif value == 'Payment':
        for row in Cart:
            a=print(printFormat.format("|", *row))
        return a
    elif value == 'Cart':
        print("\nDaftar mobil yang dipinjam")
        for row in Cart:
            a=print(printFormat.format("|", *row))
        return a
    else :
        a = 'Menu yang anda pilih tidak tersedia'
        return print(a), ChooseMenu()
def EditDatabase():
    NK = str(input("\nMasukan Nomor Kendaraan : "))
    NK = NK.upper()
    NK = re.sub("\s",'', NK)
    sementara = []
    for i in range(len(Database)):
        if Database[i][0] == NK:
            Brand = str(input("Masukan Brand Kendaraan : ")).upper()
            Year = int(input("Masukan Year Kendaraan : "))
            Prize = int(input("Masukan Prize Kendaraan : "))
            print('''
                == Pilih Status Ketersediaan ==
                1. Available
                2. Not Avail
            ''')
            Status = int(input("Masukan Status Kendaraan : "))
            if Status == 1:
                Status = 'AVAILABLE'
            elif Status == 2:
                Status = 'NOT AVAIL'
            else :
                print("\nAngka yang anda pilih tidak ada !")
                return EditDatabase()
            Database[i][1]=Brand
            Database[i][2]=Year
            Database[i][3]=Prize
            Database[i][4]=Status
            a = print(Brand, Year, Prize, Status, 'Berhasil Ditambahkan !')
            ShowData('EditDatabase')
            break
    if Database[i][0] != NK:
        a = str(input(f"\nNomor Kendaraan Tidak Tersedia. Anda ingin menambahkan {NK} sebagai data baru ? (Y/N)")).upper()
        if a == 'Y':
            return AddToDatabase()
        else:
            return ChooseMenu()
        a = print('Nomor Kendaraan Tidak Tersedia. Silahkan Tambahkan Data Kendaraan Terlebih Dahulu !')
    return a
def AddToDatabase():
    NK = str(input("Masukan Nomor Kendaraan : "))
    NK = NK.upper()
    NK = re.sub("\s",'', NK)
    for i in range(len(Database)):
        if Database[i][0] == NK:
            a = str(input(f"Nomor kendaraan sudah tersedia dalam database ! Apakah anda ingin merubah data {NK} ? (Y/N)")).upper()
            if a == 'Y':
                return EditDatabase()
            else:
                return ChooseMenu()
    if Database[i][0] != NK:
        Brand = str(input("Masukan Brand Kendaraan : ")).upper()
        Year = int(input("Masukan Year Kendaraan : "))
        Prize = int(input("Masukan Prize Kendaraan : "))
        print('''
            == Pilih Status Ketersediaan ==
            1. Available
            2. Not Avail
        ''')
        Status = int(input("Masukan Status Kendaraan : "))
        if Status == 1:
            Status = 'AVAILABLE'
        elif Status == 2:
            Status = 'NOT AVAIL'
        else :
            print("Angka yang anda pilih tidak ada !")
            return AddToDatabase()
        print(f'\nNK\t:{NK}\nBrand\t:{Brand}\nYear\t:{Year}\nPrize\t:{Prize}\nStatus\t:{Status}')
        valids = str(input('Apakah data sudah sesuai ? (Y/N)')).upper()
        if valids == 'Y':
            Database.append([NK,Brand,Year,Prize,Status])
            a = print(NK, Brand, Year, Prize, Status, 'Berhasil Ditambahkan !')
        else:
            AddToDatabase()
    return a, ShowData('AddToDatabase')

def DeleteDatabase(NK):
    NK = NK.upper()
    NK = re.sub("\s",'', NK)
    for i in range(len(Database)):
        if NK == Database[i][0]:
            validation = str(input(f'Yakin ingin menghapus {NK} dari database ? (Y/N)')).upper()
            if validation == 'Y':
                Database.remove(Database[i])
                a='Data berhasil dihapus'
                break
            elif validation == 'N' :
                a='Data tidak terhapus'
                break
            else :
                a='Pilihan anda tidak tersedia'
        else:
            a='Data tidak ditemukan'
    return print(a), ShowData('DeleteDatabase')

def AddToCart():
    NK = str(input("Masukan Nomor Kendaraan : "))
    NK = NK.upper()
    NK = re.sub("\s",'', NK)
    for i in range(len(Database)):
        if Database[i][0] == NK:
            if Database[i][4] == 'AVAILABLE':
                Brand = Database[i][1]
                Year = Database[i][2]
                Prize = Database[i][3]
                Database[i][4] = 'NOT AVAIL'
                From = str(input("Masukan Tanggal Peminjaman : "))
                Until = str(input("Masukan Tanggal Pengembaliaan : "))
                Cart.append([NK,Brand,From,Until,Prize])
                break
            else:
                print("Mobil yang anda inginkan sedang disewakan !")
                return AddToCart()
        else:
            continue
    return ShowData('Cart')

def Payment(Total):
    NK = str(input("Masaukan Nomor Kendaraan : "))
    NK = NK.upper()
    NK = re.sub("\s",'', NK)
    for i in range(len(Database)):
        if Database[i][0] == NK:
            Total = Database[i][3]
            Bayar = int(input("masukan jumlah pembayaran : "))
            if (Total > Bayar ):
                print("Transaksi Dibatalkan")
                Total = Total-Bayar
                print("pembayaran kurang sebesar","{:,}".format(Total))
                Total = 0
                Payment(Total)
                break
            else:
                print("Transaksi Berhasil")
                Total = Bayar-Total
                print("Terima Kasih")
                print("uang kembalian anda","{:,}".format(Total))
                Database[i][4] = 'AVAILABLE'
                break
    for i in range(len(Cart)):
        if NK == Cart[i][0]:
            Cart.remove(Cart[i])
            a=''
            break
        else:
           a='Data tidak ditemukan'
    return ChooseMenu()

ChooseMenu()